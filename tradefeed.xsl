<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" indent="yes"/>

    <xsl:variable name="tradeType" >TRADE_PRINCIPAL_SECURITY</xsl:variable>
    <xsl:param name="infernoTradePrefix">PLACEHOLDER-</xsl:param>

    <xsl:template match="trade">
        <xsl:variable name="contractRef" select="concat($infernoTradePrefix, contract_ref)"/>

        <xsl:variable name="cs_duplicate_unique_ref" select="concat(contract_ref, '-', version)"/>

        <xsl:variable name="negate">
            <xsl:choose>
                <xsl:when test="buy_sell = 'B'">   <!-- we are buying -->
                    <xsl:value-of select="number(1)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="number(-1)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <TRADE>    
            <xsl:attribute name="Reference">
                <xsl:value-of select="$contractRef"/>
            </xsl:attribute>

            <xsl:variable name="tradeAction">
                <xsl:choose>
                    <xsl:when test="is_cancel = 'true'"><xsl:text>CANCEL</xsl:text></xsl:when>
                    <xsl:when test="number(version) &gt; 1"><xsl:text>AMEND</xsl:text></xsl:when>
                    <xsl:otherwise><xsl:text>NEW</xsl:text></xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            
            <xsl:attribute name="unique_id">
                <xsl:value-of select="unique_id"/>
            </xsl:attribute>

            <xsl:attribute name="cs_contract_ref">
                <xsl:value-of select="contract_ref"/>
            </xsl:attribute>

            <xsl:attribute name="cs_duplicate_unique_ref">
                <xsl:value-of select="$cs_duplicate_unique_ref"/>
            </xsl:attribute>

            <xsl:element name="{$tradeType}">
                <xsl:attribute name="ACTION">
                    <xsl:value-of select="$tradeAction"/>
                </xsl:attribute>

                <xsl:attribute name="cs_duplicate_unique_ref">
                    <xsl:value-of select="$cs_duplicate_unique_ref"/>
                </xsl:attribute>

                <MSG_ID>
                    <NAMESPACE>CS</NAMESPACE>
                    <EXTERNAL_ID><xsl:value-of select="$contractRef"/></EXTERNAL_ID>
                </MSG_ID>

                <xsl:element name="{concat($tradeType, '_INPUT')}">
                    <CONTRACT_REF>
                        <ID TYPE="PRIMARY" CLASS="ContractRef"><xsl:value-of select="$contractRef"/></ID>
                    </CONTRACT_REF>
                    
                    <COUNTERPARTY>
                        <ID TYPE="InternalAccountId" CLASS="Account"><xsl:value-of select="counterparty_id"/></ID>
                    </COUNTERPARTY>

                    <BOOK>
                        <ID CLASS="Book" TYPE="INTERNAL">
                            <xsl:value-of select="book_id"/>
                        </ID>
                    </BOOK>

                    <PRICE TYPE="UC" MULT_DIV="MULT">
                        <NUMBER><xsl:value-of select="dealt_price"/></NUMBER>
                        <FROM_SECURITY>
                            <ID CLASS="SecurityInMarket" TYPE="COMPOUND">
                                <ID CLASS="Security" TYPE="CUSIP"><xsl:value-of select="instrument_CUSIP"/></ID>
                                <ID CLASS="Market" TYPE="ISO">
                                    <xsl:choose>
                                        <xsl:when test="execution_venue_id = 'US'">XUSX</xsl:when>
                                        <xsl:when test="execution_venue_id = 'CA'">
                                            <xsl:choose>
                                                <xsl:when test="dealt_ccy = 'CAD'">XCAX</xsl:when>
                                                <xsl:when test="dealt_ccy = 'USD'">XCAU</xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:message terminate="yes">
                                                        Market region issue for price. Invalid currency <xsl:value-of select="dealt_ccy"/> for market <xsl:value-of select="execution_venue_id"/>
                                                    </xsl:message>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:message terminate="yes">Invalid execution_venue_id <xsl:value-of select="execution_venue"/></xsl:message>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </ID>
                            </ID>
                        </FROM_SECURITY>
                        <TO_CURRENCY>
                            <ID TYPE="ISO" CLASS="Currency"><xsl:value-of select="dealt_ccy"/></ID>
                        </TO_CURRENCY>
                    </PRICE>

                    <QUANTITIES>
                        <QUANTITY TYPE="NumberOfUnits">
                            <NUMBER><xsl:value-of select="quantity * $negate"/></NUMBER>
                            <ID CLASS="SecurityInMarket" TYPE="COMPOUND">
                                <ID CLASS="Security" TYPE="CUSIP"><xsl:value-of select="instrument_CUSIP"/></ID>
                                <ID CLASS="Market" TYPE="ISO">
                                    <xsl:choose>
                                        <xsl:when test="execution_venue_id = 'US'">XUSX</xsl:when>
                                        <xsl:when test="execution_venue_id = 'CA'">
                                            <xsl:choose>
                                                <xsl:when test="dealt_ccy = 'CAD'">XCAX</xsl:when>
                                                <xsl:when test="dealt_ccy = 'USD'">XCAU</xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:message terminate="yes">
                                                        Market region issue. Invalid currency <xsl:value-of select="dealt_ccy"/> for market <xsl:value-of select="execution_venue_id"/>
                                                    </xsl:message>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:message terminate="yes">Invalid execution_venue_id <xsl:value-of select="execution_venue"/></xsl:message>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </ID>
                            </ID>
                        </QUANTITY>
                        <xsl:if test="number(commission_received) = commission_received and number(commission_received) != 0">
                            <QUANTITY TYPE="CommissionReceived">
                                <NUMBER><xsl:value-of select="commission_received"/></NUMBER>
                                <ID TYPE="ISO" CLASS="Currency"><xsl:value-of select="dealt_ccy"/></ID>
                            </QUANTITY>
                        </xsl:if>
                        <xsl:if test="number(commision_paid) = commission_paid and number(commision_paid) != 0">
                            <QUANTITY TYPE="CommissionPaid">
                                <NUMBER><xsl:value-of select="commission_paid"/></NUMBER>
                                <ID TYPE="ISO" CLASS="Currency"><xsl:value-of select="dealt_ccy"/></ID>
                            </QUANTITY>
                        </xsl:if>
                    </QUANTITIES>

                    <TRADE_DATE>
                        <DATETIME TZ="US/Eastern"><xsl:value-of select="translate(trade_date_time, '-', '')"/></DATETIME>
                    </TRADE_DATE>

                    <VALUE_DATE>
                        <DATETIME TZ="US/Eastern"><xsl:value-of select="translate(value_date, '-', '')"/></DATETIME>
                    </VALUE_DATE>
                    <!--<xsl:if test="number(exchange_rate) != 0">-->
                    <xsl:if test="number(exchange_rate) != 'NaN'">
                        <EXCHANGE_RATES>
                            <PRICE TYPE="ER" MULT_DIV="MULT">
                                <NUMBER><xsl:value-of select="exchange_rate"/></NUMBER>
                                <FROM_SECURITY>
                                    <!-- changed by client request
                                         <ID TYPE="ISO" CLASS="Currency"><xsl:value-of select="dealt_ccy"/></ID>
                                    -->
                                    <ID TYPE="ISO" CLASS="Currency">USD</ID>
                                </FROM_SECURITY>
                                <TO_CURRENCY>
                                    <!-- changed by client request
                                        <ID TYPE="ISO" CLASS="Currency"><xsl:value-of select="settlement_ccy"/></ID>
                                    -->
                                    <ID TYPE="ISO" CLASS="Currency">CAD</ID>
                                </TO_CURRENCY>
                            </PRICE>
                        </EXCHANGE_RATES>
                    </xsl:if>

                    <SETTLEMENT_CURRENCY>
                        <ID TYPE="ISO" CLASS="Currency"><xsl:value-of select="settlement_ccy"/></ID>
                    </SETTLEMENT_CURRENCY>

                    <TAGS>
                        <TAG NAME="SourceSystem"><xsl:value-of select="source_system_id"/></TAG>
                        <TAG NAME="BlotterCode">
                            <xsl:choose>
                                <!-- this test for DX trailer is a temporary workaround. It should be removed
                                     The otherwise case should be the default once CS implement a proper blotter code -->
                                <xsl:when test="trailer_codes/trailer_code[text() = 'DX'] and blotter_code ='TL'">64</xsl:when>
                                <xsl:otherwise><xsl:value-of select="blotter_code"/></xsl:otherwise>
                            </xsl:choose>
                        </TAG>
                        <TAG NAME="CSContractRef"><xsl:value-of select="contract_ref"/></TAG>
                        <TAG NAME="CSInstrumentId"><xsl:value-of select="instrument_id"/></TAG>
                        <TAG NAME="CSClientUsage"><xsl:value-of select="client_usage"/></TAG>
                        <TAG NAME="BuySellType"><xsl:value-of select="buy_sell"/></TAG>
                        <TAG NAME="SourceOrderNo"><xsl:value-of select="source_order_no"/></TAG>
                        <TAG NAME="SoftCommissionIndicator">
                            <xsl:choose>
                                <xsl:when test="is_soft_commission = 'Y'">T</xsl:when>
                                <xsl:otherwise>F</xsl:otherwise>
                            </xsl:choose>
                        </TAG>
                        <TAG NAME="ClientAcro"><xsl:value-of select="client_acro"/></TAG>
                        <TAG NAME="RegisteredRepresentative"><xsl:value-of select="rr_code"/></TAG>
                        <TAG NAME="RegisteredRepresentativeSecondary"><xsl:value-of select="rr_code_secondary"/></TAG>

                        <xsl:if test="memo_txt/memo_1">
                            <TAG NAME="ConfirmationMemo1Text"><xsl:value-of select="memo_txt/memo_1"/></TAG>
                        </xsl:if>
                        <xsl:if test="memo_txt/memo_2">
                            <TAG NAME="ConfirmationMemo2Text"><xsl:value-of select="memo_txt/memo_2"/></TAG>
                        </xsl:if>
                        <xsl:if test="memo_txt/memo_3">
                            <TAG NAME="ConfirmationMemo3Text"><xsl:value-of select="memo_txt/memo_3"/></TAG>
                        </xsl:if>
                        <xsl:if test="memo_txt/memo_4">
                            <TAG NAME="ConfirmationMemo4Text"><xsl:value-of select="memo_txt/memo_4"/></TAG>
                        </xsl:if>
                        <xsl:if test="memo_txt/memo_5">
                            <TAG NAME="ConfirmationMemo5Text"><xsl:value-of select="memo_txt/memo_5"/></TAG>
                        </xsl:if>
                        <xsl:if test="memo_txt/memo_6">
                            <TAG NAME="ConfirmationMemo6Text"><xsl:value-of select="memo_txt/memo_6"/></TAG>
                        </xsl:if>

                        <!-- process trailers -->
                        <xsl:if test="trailer_codes/trailer_code[text() = 'S']">
                            <TAG NAME="ShortSellIndicator">T</TAG>
                        </xsl:if>
                        <xsl:if test="trailer_codes/trailer_code[text() = '94']">
                            <TAG NAME="NCIBindicator">T</TAG>
                        </xsl:if>
                        <xsl:if test="trailer_codes/trailer_code[text() = 'W4']">
                            <TAG NAME="IfAsWhenIssuedIndicator">T</TAG>
                        </xsl:if>
                        <xsl:if test="trailer_codes/trailer_code[text() = 'SC' or text() = 'UN']">
                            <xsl:if test="trailer_codes/trailer_code[text() = 'SC'] and
                                          trailer_codes/trailer_code[text() = 'UN']">
                                <xsl:message terminate="yes">
                                    Invalid solicited trailer codes. Cannot have code SC and UN specified at the same time.
                                </xsl:message>
                            </xsl:if>
                            <xsl:if test="trailer_codes/trailer_code[text() = 'SC']">
                                <TAG NAME="SolicitedIndicator">T</TAG>
                            </xsl:if>
                            <xsl:if test="trailer_codes/trailer_code[text() = 'UN']">
                                <TAG NAME="SolicitedIndicator">F</TAG>
                            </xsl:if>
                        </xsl:if>
                        <xsl:if test="trailer_codes/trailer_code[text() = '90' or text() = '93']">
                            <xsl:if test="trailer_codes/trailer_code[text() = '90'] and
                                          trailer_codes/trailer_code[text() = '93']">
                                <xsl:message terminate="yes">
                                    Invalid trading capacity trailer codes. Cannot have code 90 and 93 specified at the same time.
                                </xsl:message>
                            </xsl:if>
                            <xsl:if test="trailer_codes/trailer_code[text() = '90']">
                                <TAG NAME="TradingCapacity">P</TAG>
                            </xsl:if>
                            <xsl:if test="trailer_codes/trailer_code[text() = '93']">
                                <TAG NAME="TradingCapacity">A</TAG>
                            </xsl:if>
                        </xsl:if>
                        <!-- end trailer processing -->


                    </TAGS>
                </xsl:element>
            </xsl:element>
        </TRADE>
    </xsl:template>

    <xsl:template match="/">
        <TRADES>
            <xsl:apply-templates/>
        </TRADES>
    </xsl:template>

</xsl:stylesheet>
